﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using xps2img;

namespace ConsoleTestApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var xpsConverter = new Xps2Image(@"E:\backup\test.xps"))
            {
                IEnumerable<Bitmap> images = xpsConverter.ToBitmap(new Parameters
                {
                    ImageType = ImageType.Png,
                    Dpi = 300
                });

                int i = 0;

                foreach(Bitmap image in images)
                {
                    image.Save(@"E:\backup\file_" + i.ToString() + ".png", ImageFormat.Png);
                    i++;
                }

                Console.WriteLine(images.GetType());
            }

            Console.Read();
        }
    }
}
